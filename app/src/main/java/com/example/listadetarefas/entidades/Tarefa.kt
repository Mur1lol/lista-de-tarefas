package com.example.listadetarefas.entidades

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tarefas")
data class Tarefa(
    var titulo: String,
    var descricao: String,
    var status: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    val tarefaCompleta get() = "$titulo \n$status$descricao"
    override fun toString() = tarefaCompleta
}