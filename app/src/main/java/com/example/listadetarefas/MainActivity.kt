package com.example.listadetarefas

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.room.Room
import com.example.listadetarefas.bd.AppDatabase
import com.example.listadetarefas.bd.dao.TarefaDao
import com.example.listadetarefas.entidades.Tarefa
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    lateinit var db: AppDatabase
    lateinit var tarefaDao: TarefaDao
    lateinit var adapter: ArrayAdapter<Tarefa>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java,
            "tarefa.db"
        )
            .allowMainThreadQueries()
            .addMigrations()
            .build()
        tarefaDao = db.tarefaDao()

        btEnviar.setOnClickListener {
            val titulo = txtTitulo.text.toString()
            val descricao = txtDescricao.text.toString()
            val status = ""

            val tarefa = Tarefa(titulo, descricao, status)

            tarefaDao.inserir(tarefa)
            atualizaLista()
            limpaCampos()
        }
        atualizaLista()

        listTarefas.setOnItemClickListener { _, _, position, _ ->
            val tarefa = adapter.getItem(position)
            txtTitulo.setText(tarefa.titulo)
            txtDescricao.setText(tarefa.descricao)
            tarefa.status = ""

            btDeletar.setOnClickListener {
                if (tarefa.titulo == "" && tarefa.descricao == "") {
                    limpaCampos()
                }
                else {
                    tarefaDao.apagar(tarefa)
                    Toast.makeText(this, "Tarefa Apagada!", Toast.LENGTH_SHORT).show()
                    atualizaLista()
                    limpaCampos()
                }
            }

            btEditar.setOnClickListener {
                tarefa.titulo = txtTitulo.text.toString()
                tarefa.descricao = txtDescricao.text.toString()
                tarefa.status = ""

                if (tarefa.titulo == "" && tarefa.descricao == "") {
                    limpaCampos()
                }
                else {
                    tarefaDao.atualizar(tarefa)
                    Toast.makeText(this, "Tarefa Atualizada!", Toast.LENGTH_SHORT).show()
                    atualizaLista()
                    limpaCampos()
                }
            }
        }
        atualizaLista()

        listTarefas.setOnItemLongClickListener { _, _, position, _ ->
            val tarefa = adapter.getItem(position)
            tarefa.status = "[FEITO] - "

            tarefaDao.atualizar(tarefa)
            Toast.makeText(this, "Tarefa Concluida!", Toast.LENGTH_SHORT).show()
            atualizaLista()
            limpaCampos()
            true
        }
        atualizaLista()

    }

    fun atualizaLista(){
        val tarefas = tarefaDao.buscaTodas()
        adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, tarefas)
        listTarefas.adapter = adapter
    }

    fun limpaCampos(){
        txtTitulo.setText("")
        txtDescricao.setText("")
    }
}
