package com.example.listadetarefas.bd.dao

import androidx.room.*
import com.example.listadetarefas.entidades.Tarefa

@Dao
interface TarefaDao {
    @Query("SELECT * FROM tarefas")
    fun buscaTodas(): List<Tarefa>

    @Query("SELECT * FROM tarefas WHERE id = :id LIMIT 1")
    fun buscaTarefa(id: Int): Tarefa

    @Update
    fun atualizar(tarefa: Tarefa)

    @Insert
    fun inserir(tarefa: Tarefa)

    @Delete
    fun apagar(tarefa: Tarefa)

}