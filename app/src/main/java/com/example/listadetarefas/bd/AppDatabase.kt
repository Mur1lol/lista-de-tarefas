package com.example.listadetarefas.bd

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.listadetarefas.bd.dao.TarefaDao
import com.example.listadetarefas.entidades.Tarefa

@Database(entities = arrayOf(Tarefa::class), version = 1)
abstract class AppDatabase: RoomDatabase() {
    abstract fun tarefaDao(): TarefaDao
}